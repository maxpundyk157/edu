<?php


namespace Divan\CustomReview\Api;
/**
 * Interface ReviewRepositoryInterface
 * @package Divan\CustomReview\Api
 * @api
 */
interface ReviewRepositoryInterface
{
    /**
     * Save review.
     *
     * @param \Divan\CustomReview\Api\Data\ReviewInterface $review
     * @return \Divan\CustomReview\Api\Data\ReviewInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(Data\ReviewInterface $review);

    /**
     * Retrieve review.
     *
     * @param int $reviewId
     * @return \Divan\CustomReview\Api\Data\ReviewInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($reviewId);

    /**
     * Get review list.
     *
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Magento\Framework\Api\SearchResults
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria);

    /**
     * Delete review.
     *
     * @param \Divan\CustomReview\Api\Data\ReviewInterface $review
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(Data\ReviewInterface $review);

    /**
     * Delete review by ID.
     *
     * @param int $reviewId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($reviewId);

    /**
     * @return array
     */
    public function deleteOldReviews();
}
