<?php

namespace Divan\CustomReview\Api\Data;

/**
 * Interface ReviewInterface
 * @package Divan\CustomReview\Api\Data
 * @api
 */
interface ReviewInterface
{
    const TABLE_NAME = 'divan_custom_review';
    const REVIEW_ID = 'review_id';
    const IS_APPROVED = 'isApproved';
    const PRODUCT_ID = 'product_id';
    const CUSTOMER_ID = 'customer_id';
    const CONTENT = 'content';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * Get review ID
     * @return int|null
     */
    public function getReviewId();

    /**
     * Get is approved
     * @return boolean|null
     */
    public function getIsApproved();

    /**
     * Get product ID
     * @return int|null
     */
    public function getProductId();

    /**
     * Get customer ID
     * @return int|null
     */
    public function getCustomerId();

    /**
     * Get content
     * @return string|null
     */
    public function getContent();

    /**
     * Get Created At
     *
     * @return string|null
     */
    public function getCreatedAt();

    /**
     * Get Updated At
     *
     * @return string|null
     */
    public function getUpdatedAt();

    /**
     * Set ID
     *
     * @param int $reviewId
     * @return ReviewInterface
     */
    public function setReviewId($reviewId);

    /**
     * Set is approved
     *
     * @param boolean $isApproved
     * @return ReviewInterface
     */
    public function setIsApproved($isApproved);

    /**
     * Set content
     *
     * @param string $content
     * @return ReviewInterface
     */
    public function setContent($content);

    /**
     * Set product ID
     *
     * @param int $productId
     * @return ReviewInterface
     */
    public function setProductId($productId);

    /**
     * Set customer ID
     *
     * @param int $customerId
     * @return ReviewInterface
     */
    public function setCustomerId($customerId);
}
