<?php


namespace Divan\CustomReview\Api\Data;
/**
 * Interface ReviewSearchResultsInterface
 * @package Divan\CustomReview\Api\Data
 * @api
 */

interface ReviewSearchResultsInterface
{
    /**
     * Get review list.
     *
     * @return \Divan\CustomReview\Api\Data\ReviewInterface[]
     */
    public function getReviews();

    /**
     * Set review list.
     *
     * @param \Divan\CustomReview\Api\Data\ReviewInterface[] $reviews
     * @return $this
     */
    public function setReviews(array $reviews);

}
