<?php

namespace Divan\CustomReview\Block\Review;

use Divan\CustomReview\Model\Config;
use Divan\CustomReview\Model\ResourceModel\Review\CollectionFactory;
use Magento\Catalog\Block\Product\Context;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Data\Form\FormKey;
use Magento\Framework\Stdlib\ArrayUtils;

/**
 * Class View
 * @package Divan\CustomReview\Block\Review
 */

class View extends \Magento\Catalog\Block\Product\View\AbstractView
{
    const PAGER_THREE = 3;
    const PAGER_FIVE = 5;
    const PAGER_TEN = 10;
    const PAGER_TWENTY = 20;

    protected $collection;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;
    /**
     * @var \Magento\Framework\App\Http\Context
     */
    private $httpContext;
    protected $customer;
    /**
     * @var FormKey
     */
    protected $formKey;
    /**
     * @var Config
     */
    protected $config;
    /**
     * @var ScopeConfigInterface
     */
    protected $_scopeConfig;

    /**
     * View constructor.
     * @param CollectionFactory $collectionFactory
     * @param Context $context
     * @param ArrayUtils $arrayUtils
     * @param \Magento\Framework\App\Http\Context $httpContext
     * @param Session $customer
     * @param FormKey $formKey
     * @param Config $config
     * @param ScopeConfigInterface $_scopeConfig
     * @param array $data
     */
    public function __construct(
        CollectionFactory $collectionFactory,
        Context $context,
        ArrayUtils $arrayUtils,
        \Magento\Framework\App\Http\Context $httpContext,
        Session $customer,
        FormKey $formKey,
        Config $config,
        ScopeConfigInterface $_scopeConfig,
        array $data = []
    ) {
        parent::__construct($context, $arrayUtils, $data);
        $this->collectionFactory = $collectionFactory;
        $this->httpContext = $httpContext;
        $this->customer = $customer;
        $this->formKey = $formKey;
        $this->config = $config;
        $this->_scopeConfig =$_scopeConfig;
    }

    /**
     * @return $this|View
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();

        if ($this->getReviews()) {
            $pager = $this->getLayout()->createBlock(
                'Magento\Theme\Block\Html\Pager',
                'pager'
            )->setAvailableLimit([self::PAGER_THREE => self::PAGER_THREE, self::PAGER_FIVE => self::PAGER_FIVE, self::PAGER_TEN => self::PAGER_TEN, self::PAGER_TWENTY => self::PAGER_TWENTY])->setShowPerPage(true)->setCollection(
                $this->getReviews()
            );
            $this->setChild('pager', $pager);
            $this->getReviews()->load(); //?
        }
        return $this;
    }

    /**
     * @return string
     */
    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    /**
     * @return \Divan\CustomReview\Model\ResourceModel\Review\Collection
     */

    public function getReviews()
    {
        if ($this->collection) {
            return $this->collection;
        }
        $page = ($this->getRequest()->getParam('p')) ? $this->getRequest()->getParam('p') : 1;
        $pageSize = ($this->getRequest()->getParam('limit')) ? $this->getRequest()->getParam('limit') : self::PAGER_THREE;

        $collection = $this->collectionFactory->create();
        $collection->addFieldToFilter('product_id', ['eq' => $this->getProductId()]);
        $collection->getSelect()->joinLeft(
            ['customer_entity' => $collection->getTable('customer_entity')],
            'main_table.customer_id = customer_entity.entity_id',
            ['first_name' => 'customer_entity.firstname', 'last_name' => 'customer_entity.lastname']
        );

        $collection->setCurPage($page);
        $collection->setPageSize($pageSize);

        $this->collection = $collection;
        return $this->collection;
    }

    /**
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getFormKey()
    {
        return $this->formKey->getFormKey();
    }

    /**
     * Retrieve current product model from registry
     *
     * @return \Magento\Catalog\Model\Product
     */
    public function getProductId()
    {
        return $this->_coreRegistry->registry('current_product')->getId();
    }

    /**
     * Retrieve current product model from registry
     *
     * @return string
     */
    public function getCustomer()
    {
        return $this->customer->getCustomer();
    }

    /**
     * Retrieve current product model from registry
     *
     * @return bool
     */
    public function getIsCustomerLoggedIn()
    {
        return $this->httpContext->getValue(\Magento\Customer\Model\Context::CONTEXT_AUTH);
    }

    /**
     * @return int
     */
    public function getReviewsCount()
    {
        return $this->getReviews()->getSize();
    }

    /**
     * @return bool
     */
    public function getIsUserViewEnabled()
    {
        return $this->_scopeConfig->getValue(
            Config::XML_PATH_ENABLED_USER_VIEW,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }




}
