<?php
namespace Divan\CustomReview\Block\Adminhtml\Buttons;

use Divan\CustomReview\Api\ReviewRepositoryInterface;

use Magento\Backend\Block\Widget\Context;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class GenericButton
 * @package Divan\CustomReview\Block\Adminhtml\Buttons
 */
class GenericButton
{
    /**
     * @var Context
     */
    protected $context;

    /**
     * @var ReviewRepositoryInterface
     */
    private $reviewRepository;

    /**
     * @param Context $context
     * @param ReviewRepositoryInterface $reviewRepository
     */
    public function __construct(
        Context $context,
        ReviewRepositoryInterface $reviewRepository
    ) {
        $this->context = $context;
        $this->reviewRepository = $reviewRepository;
    }

    /**
     * @return int|null
     */
    public function getReviewId()
    {
        try {
            return $this->reviewRepository->getById(
                $this->context->getRequest()->getParam('review_id')
            )->getId();
        } catch (NoSuchEntityException $e) {
            return null;
        }
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
