<?php

namespace Divan\CustomReview\Controller\Adminhtml\Index;

use Divan\CustomReview\Api\ReviewRepositoryInterface;
use Divan\CustomReview\Model\ReviewFactory;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Result\PageFactory;

/**
 * Class Index
 * @package Divan\CustomReview\Controller\Adminhtml\Index
 */
class Edit extends AbstractAction
{
    /**
     * @var PageFactory
     */
    private $resultPageFactory;
    /**
     * @var ReviewRepositoryInterface
     */
    private $reviewRepository;
    /**
     * @var ReviewFactory
     */
    private $reviewFactory;
    /**
     * @var \Magento\Framework\Registry
     */
    private $coreRegistry;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     * @param ReviewRepositoryInterface $reviewRepository
     * @param ReviewFactory $reviewFactory
     */

    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        ReviewFactory $reviewFactory,
        \Magento\Framework\Registry $coreRegistry,
        ReviewRepositoryInterface $reviewRepository
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->reviewFactory = $reviewFactory;
        $this->reviewRepository = $reviewRepository;
        $this->coreRegistry = $coreRegistry;

        parent::__construct($context);
    }

    /**
     * Index action
     * @return ResultInterface
     */
    public function execute()
    {
        $id = $this->getRequest()->getParam('review_id'); // unique key need to be pass to DataProvider
        if ($id) {
            try {
                $model = $this->reviewRepository->getById($id);
            } catch (NoSuchEntityException $exception) {
                $this->messageManager->addErrorMessage(__($exception->getMessage()));
                /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        } else {
            $model = $this->reviewFactory->create();
        }
        $this->coreRegistry->register('divan_custom_review', $model);

        /** @var ResultInterface $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Divan_CustomReview::review')->addBreadcrumb(__('Customer Review'), __('Customer Review'));

        $resultPage->getConfig()->getTitle()->set(__("Review")); // set title in head
        $resultPage->getConfig()->getTitle()->prepend($model->getId() ? "Review ID: " . $model->getId() : __('New Review')); // set title on page

        return $resultPage;
    }
}
