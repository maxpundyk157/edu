<?php

namespace Divan\CustomReview\Controller\Adminhtml\Index;

use Divan\CustomReview\Api\ReviewRepositoryInterface;
use Divan\CustomReview\Model\ResourceModel\Review\CollectionFactory;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Ui\Component\MassAction\Filter;

/**
 * Class MassDelete
 * @package Divan\CustomReview\Controller\Adminhtml\Index
 */
class MassDelete extends AbstractAction
{
    /**
     * @var DataPersistorInterface
     */
    private $dataPersistor;

    /**
     * @var ReviewRepositoryInterface
     */
    private $reviewRepository;

    /**
     * @var Filter
     */
    private $filter;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @param Context $context
     * @param ReviewRepositoryInterface $reviewRepository
     * @param CollectionFactory $collectionFactory
     * @param Filter $filter
     * @param DataPersistorInterface $dataPersistor
     */
    public function __construct(
        Context $context,
        ReviewRepositoryInterface $reviewRepository,
        CollectionFactory $collectionFactory,
        Filter $filter,
        DataPersistorInterface $dataPersistor
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->filter = $filter;
        $this->reviewRepository = $reviewRepository;
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\Controller\ResultInterface
     */

    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();

        $collection = $this->filter->getCollection($this->collectionFactory->create());
        try {
            foreach ($collection as $review) {
                $this->reviewRepository->delete($review);
            }
            $message = __('A total of %1 record(s) have been deleted.', $collection->count());
            $this->messageManager->addSuccessMessage($message);
            $this->dataPersistor->clear('review');
            return $resultRedirect->setPath('*/*/');
        } catch (NoSuchEntityException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        } catch (LocalizedException $e) {
            $this->messageManager->addErrorMessage($e->getMessage());
        } catch (\Exception $e) {
            $this->messageManager->addExceptionMessage($e, __('Something went wrong while deleting reviews.'));
        }

        return $resultRedirect->setPath('*/*/');
    }

}
