<?php

namespace Divan\CustomReview\Controller\Adminhtml\Index;

use Divan\CustomReview\Api\Data\ReviewInterface;
use Divan\CustomReview\Api\ReviewRepositoryInterface;
use Divan\CustomReview\Model\ReviewFactory;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class Save
 * @package Divan\CustomReview\Controller\Adminhtml\Index
 */
class Save extends AbstractAction
{
    /**
     * @var DataPersistorInterface
     */
    private $dataPersistor;
    /**
     * @var ReviewRepositoryInterface
     */
    private $reviewRepository;
    /**
     * @var ReviewFactory
     */
    private $reviewFactory;

    /**
     * Save constructor.
     * @param Context $context
     * @param ReviewRepositoryInterface $reviewRepository
     * @param ReviewFactory $reviewFactory
     * @param DataPersistorInterface $dataPersistor
     */
    public function __construct(
        Context $context,
        ReviewRepositoryInterface $reviewRepository,
        ReviewFactory $reviewFactory,
        DataPersistorInterface $dataPersistor
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->reviewRepository = $reviewRepository;
        $this->reviewFactory = $reviewFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        if ($data) {
            $id = $this->getRequest()->getParam(ReviewInterface::REVIEW_ID);

            if (empty($data[ReviewInterface::REVIEW_ID])) {
                $data[ReviewInterface::REVIEW_ID] = null;
            }

            if ($id) {
                try {
                    /** @var ReviewInterface $model */
                    $model = $this->reviewRepository->getById($id);
                } catch (NoSuchEntityException $e) {
                    $this->messageManager->addErrorMessage(__($e->getMessage()));
                    /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                    $resultRedirect = $this->resultRedirectFactory->create();
                    return $resultRedirect->setPath('*/*/');
                }
            } else {
                /** @var ReviewInterface $model */
                $model = $this->reviewFactory->create();
            }
            $model->setData($data);

            try {
                $this->reviewRepository->save($model);
                $this->messageManager->addSuccessMessage(__('You saved the review.'));
                $this->dataPersistor->clear('review');

                return $resultRedirect->setPath('*/*/');
            } catch (CouldNotSaveException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the review.'));
            }

            $this->dataPersistor->set('review', $data);
            return $resultRedirect->setPath('*/*/edit', [ReviewInterface::REVIEW_ID => $id]);
        }
        return $resultRedirect->setPath('*/*/');
    }
}
