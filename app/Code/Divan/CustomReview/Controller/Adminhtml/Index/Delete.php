<?php


namespace Divan\CustomReview\Controller\Adminhtml\Index;

use Divan\CustomReview\Api\ReviewRepositoryInterface;
use Divan\CustomReview\Model\ReviewFactory;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class Delete
 * @package Divan\CustomReview\Controller\Adminhtml\Index
 */
class Delete extends AbstractAction
{
    /**
     * @var DataPersistorInterface
     */
    private $dataPersistor;
    /**
     * @var ReviewRepositoryInterface
     */
    private $reviewRepository;

    /**
     * Save constructor.
     * @param Context $context
     * @param ReviewRepositoryInterface $reviewRepository
     * @param DataPersistorInterface $dataPersistor
     */
    public function __construct(
        Context $context,
        ReviewRepositoryInterface $reviewRepository,
        DataPersistorInterface $dataPersistor
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->reviewRepository = $reviewRepository;
        parent::__construct($context);
    }

    /**
     * @return  \Magento\Framework\Controller\ResultInterface
     */

    public function execute()
    {
        /** @var Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $id = $this->getRequest()->getParam('review_id');

        try {
            $this->reviewRepository->deleteById($id);
            $this->messageManager->addSuccessMessage(__('You deleted the review'));
            $this->dataPersistor->clear('review');
            return $resultRedirect->setPath('*/*/');
        } catch (NoSuchEntityException $exception) {
            $this->messageManager->addErrorMessage($exception->getMessage());
        } catch (LocalizedException $exception) {
            $this->messageManager->addErrorMessage($exception->getMessage());
        } catch (\Exception $exception) {
            $this->messageManager->addExceptionMessage($exception, __('Something went wrong while deleting the review.'));
        }
        return $resultRedirect->setPath('*/*/');
    }

}
