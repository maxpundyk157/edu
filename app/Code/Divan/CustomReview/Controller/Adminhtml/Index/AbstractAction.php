<?php


namespace Divan\CustomReview\Controller\Adminhtml\Index;

use Magento\Backend\App\Action;

/**
 * Class AbstractAction
 * @package Divan\CustomReview\Controller\Adminhtml\Index
 */

abstract class AbstractAction extends Action
{
    const ADMIN_RESOURCE = 'Divan_CustomReview::review';
}
