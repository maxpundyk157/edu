<?php


namespace Divan\CustomReview\Controller\Adminhtml\Index;

use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\View\Result\PageFactory;


/**
 * Class Index
 * @package Divan\CustomReview\Controller\Adminhtml\Index
 */

class Index extends AbstractAction

{
    /**
     * @var PageFactory
     */
    private $resultPageFactory;

    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * Index action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();

        $resultPage->setActiveMenu('Divan_CustomReview::review');
        $resultPage->getConfig()->getTitle()->set(__("Reviews")); // set title in head
        $resultPage->getConfig()->getTitle()->prepend(__("Reviews")); // set title on page

        return $resultPage;    }

}
