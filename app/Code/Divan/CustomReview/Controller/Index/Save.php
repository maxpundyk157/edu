<?php

namespace Divan\CustomReview\Controller\Index;

use Divan\CustomReview\Api\Data\ReviewInterface;
use Divan\CustomReview\Api\ReviewRepositoryInterface;
use Divan\CustomReview\Model\ReviewFactory;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class Index
 * @package Divan\CustomReview\etc
 */
class Save extends \Magento\Framework\App\Action\Action
{
    /**
     * @var Validator
     */
    protected $formKeyValidator;

    /**
     * @var ReviewRepositoryInterface
     */
    private $reviewRepository;
    /**
     * @var ReviewFactory
     */
    private $reviewFactory;

    public function __construct(
        Context $context,
        \Magento\Framework\Data\Form\FormKey\Validator $formKeyValidator,
        ReviewRepositoryInterface $reviewRepository,
        ReviewFactory $reviewFactory
    ) {
        $this->formKeyValidator = $formKeyValidator;
        $this->reviewRepository = $reviewRepository;
        $this->reviewFactory = $reviewFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        if ($this->formKeyValidator->validate($this->getRequest())) {
            $data = $this->getRequest()->getPostValue();
            if ($data) {
                /** @var ReviewInterface $model */
                $model = $this->reviewFactory->create();

                $model->setData($data);

                try {
                    $this->reviewRepository->save($model);
                    $this->messageManager->addSuccessMessage(__('You saved the review.'));
                    return $this->_redirect($this->_redirect->getRefererUrl());
                } catch (LocalizedException $e) {
                    $this->messageManager->addErrorMessage($e->getMessage());
                } catch (\Exception $e) {
                    $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the review.'));
                }
            }
            $this->messageManager->addErrorMessage(__('data is empty'));

            return $this->_redirect($this->_redirect->getRefererUrl());
        } else {
            return $this->messageManager->addWarningMessage(__('key validation failed'));
        }
    }
}
