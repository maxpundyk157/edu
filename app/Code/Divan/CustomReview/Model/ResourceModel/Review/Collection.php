<?php


namespace Divan\CustomReview\Model\ResourceModel\Review;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Divan\CustomReview\Model\ResourceModel\Review as ReviewResource;
use Divan\CustomReview\Model\Review;

/**
 * Class Collection
 * @package Divan\CustomReview\Model\ResourceModel\Review
 */
class Collection extends AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'review_id';

    /**
     * Define model & resource model
     */

    protected function _construct()
    {
        $this->_init(
            Review::class,
            ReviewResource::class
        );
    }

    /**
     * @param $variable
     * @return \Magento\Framework\DataObject[]
     * @throws \Exception
     */
    public function selectOldReviews($variable)
    {

        $fromTime = new \DateTime('now', new \DateTimezone('UTC'));
        $fromTime->sub(\DateInterval::createFromDateString($variable));



        $fromDate = $fromTime->format('Y-m-d H:i:s');

        return $this
            ->addFieldToFilter('created_at', ['lteq' => $fromDate])
            ->getItems();
    }
}
