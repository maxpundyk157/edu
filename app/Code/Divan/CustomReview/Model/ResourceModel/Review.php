<?php


namespace Divan\CustomReview\Model\ResourceModel;

use Divan\CustomReview\Api\Data\ReviewInterface;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * Class Review
 * @package Divan\CustomReview\Model\ResourceModel
 */
class Review extends AbstractDb
{
    protected function _construct()
    {
        $this->_init(ReviewInterface::TABLE_NAME, ReviewInterface::REVIEW_ID);
    }
}
