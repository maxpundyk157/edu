<?php

namespace Divan\CustomReview\Model;

use Divan\CustomReview\Api\Data\ReviewInterface;
use Magento\Framework\Model\AbstractModel;

/**
 * Class Review
 * @package Divan\CustomReview\Model
 */
class Review extends AbstractModel implements ReviewInterface
{
    protected $_eventPrefix = 'review_model';

    /**
     * set resource model
     */
    protected function _construct()
    {
        $this->_init('Divan\CustomReview\Model\ResourceModel\Review');
    }

    /**
     * Get review ID
     * @return int|null
     */

    public function getReviewId()
    {
        return (int)$this->getData(self::REVIEW_ID);
    }

    /**
     * Get is approved
     * @return boolean|null
     */

    public function getIsApproved()
    {
        return (boolean)$this->getData(self::IS_APPROVED);
    }

    /**
     * Get product ID
     * @return int|null
     */

    public function getProductId()
    {
        return (int)$this->getData(self::PRODUCT_ID);
    }

    /**
     * Get customer ID
     * @return int|null
     */

    public function getCustomerId()
    {
        return (int)$this->getData(self::CUSTOMER_ID);
    }

    /**
     * Get content
     * @return string|null
     */

    public function getContent()
    {
        return (string)$this->getData(self::CONTENT);
    }

    /**
     * Get Created At
     *
     * @return string|null
     */

    public function getCreatedAt()
    {
        return (string)$this->getData(self::CREATED_AT);
    }

    /**
     * Get Updated At
     *
     * @return string|null
     */

    public function getUpdatedAt()
    {
        return (string)$this->getData(self::UPDATED_AT);
    }

    /**
     * Set ID
     *
     * @param int $reviewId
     * @return ReviewInterface
     */

    public function setReviewId($reviewId)
    {
        return $this->setData(self::REVIEW_ID, $reviewId);
    }

    /**
     * Set is approved
     *
     * @param boolean $isApproved
     * @return ReviewInterface
     */

    public function setIsApproved($isApproved)
    {
        return $this->setData(self::IS_APPROVED, $isApproved);
    }

    /**
     * Set content
     *
     * @param string $content
     * @return ReviewInterface
     */
    public function setContent($content)
    {
        return $this->setData(self::CONTENT, $content);
    }

    /**
     * Set product ID
     *
     * @param int $productId
     * @return ReviewInterface
     */

    public function setProductId($productId)
    {
        return $this->setData(self::PRODUCT_ID, $productId);
    }

    /**
     * Set customer ID
     *
     * @param int $customerId
     * @return ReviewInterface
     */
    public function setCustomerId($customerId)
    {
        return $this->setData(self::CUSTOMER_ID, $customerId);
    }
}
