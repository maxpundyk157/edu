<?php

namespace Divan\CustomReview\Model;

use Divan\CustomReview\Api\Data;
use Divan\CustomReview\Api\Data\ReviewInterface;
use Divan\CustomReview\Api\ReviewRepositoryInterface;
use Divan\CustomReview\Model\ResourceModel\Review as ResourceReview;
use Divan\CustomReview\Model\ResourceModel\Review\CollectionFactory as ReviewCollectionFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface as SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Divan\CustomReview\Model\ResourceModel\Review\Grid\Collection;

class ReviewRepository implements ReviewRepositoryInterface
{
    /**
     * @var ResourceReview
     */
    protected $resource;

    /**
     * @var ReviewFactory
     */
    protected $reviewFactory;

    /**
     * @var ReviewCollectionFactory
     */
    protected $reviewCollectionFactory;
    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @var Data\ReviewSearchResultsInterface
     */
    protected $searchResultsFactory;

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var Collection
     */
    protected $collection;


    /**
     * ReviewRepository constructor.
     * @param ResourceReview $resource
     * @param ReviewFactory $reviewFactory
     * @param Data\ReviewSearchResultsInterfaceFactory $searchResultsFactory
     * @param CollectionProcessorInterface $collectionProcessor
     * @param ReviewCollectionFactory $reviewCollectionFactory
     * @param Config $config
     * @param Collection $collection
     */

    public function __construct(
        ResourceReview $resource,
        ReviewFactory $reviewFactory,
        Data\ReviewSearchResultsInterfaceFactory $searchResultsFactory,
        CollectionProcessorInterface $collectionProcessor,
        ReviewCollectionFactory $reviewCollectionFactory,
        Config $config,
        Collection $collection
    ) {
        $this->resource = $resource;
        $this->reviewFactory = $reviewFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionProcessor = $collectionProcessor;
        $this->reviewCollectionFactory = $reviewCollectionFactory;
        $this->config = $config;
        $this->collection = $collection;
    }

    /**
     * @param ReviewInterface $review
     * @return ReviewInterface|Review
     * @throws CouldNotSaveException
     */
    public function save(ReviewInterface $review)
    {
        try {
            /** @var $review \Divan\CustomReview\Model\Review */
            $this->resource->save($review);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
        return $review;
    }

    /**
     * @param int $reviewId
     * @return ReviewInterface|Review
     * @throws NoSuchEntityException
     */
    public function getById($reviewId)
    {
        $review = $this->reviewFactory->create();
        $this->resource->load($review, $reviewId);
        if (!$review->getId()) {
            throw new NoSuchEntityException(__('The store with the "%1" ID doesn\'t exist.', $review));
        }
        return $review;
    }

    /**
     * @param ReviewInterface $review
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(ReviewInterface $review)
    {
        try {
            /** @var $review \Divan\CustomReview\Model\Review */
            $this->resource->delete($review);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * @param int $reviewId
     * @return bool
     * @throws \Magento\Framework\Exception\LocalizedException
     */

    public function deleteById($reviewId)
    {
        try {
            /** @var  $reviewId \Divan\CustomReview\Model\Review */
            $this->delete($this->getById($reviewId));
        } catch (\Exception $exception) {
            throw new LocalizedException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return Data\ReviewSearchResultsInterfaceFactory
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        /** @var \Divan\CustomReview\Model\ResourceModel\Review\Collection $collection */
        $collection = $this->reviewCollectionFactory->create();

        $this->collectionProcessor->process($searchCriteria, $collection);

        /** @var Data\ReviewSearchResultsInterfaceFactory $searchResults */
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setReviews($collection->getReviews());
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * @return mixed|void
     * @throws LocalizedException
     */

    public function deleteOldReviews()
    {
        $timeRange = $this->config->getTimeRangeValue();
        $oldItems = $this->reviewCollectionFactory->create()->selectOldReviews($timeRange);

        foreach ($oldItems as $item) {
            $this->deleteById($item->getId());
        }
    }
}
