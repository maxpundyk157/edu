<?php

namespace Divan\CustomReview\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * Class Config
 * @package Divan\CustomReview\Model
 */
class Config
{
    const XML_PATH_ENABLED = 'divan/general/enabled';
    const XML_PATH_TIME_RANGE = 'divan/general/time_range';
    const XML_PATH_ENABLED_USER_VIEW = 'divan/general/enabled_user_view';

    const TABLE_CORE_CONFIG_DATA = 'core_config_data';
    const CONFIG_ID_CORE_CONFIG_DATA = 'config_id';
    const PATH_IN_CORE_CONFIG_DATA = 'divan/general/enabled_user_view';
    const VALUE_IN_CORE_CONFIG_DATA = 'value';


    /**
     * Config constructor.
     * @param ScopeConfigInterface $config
     */
    private $config;

    /**
     * @var \Magento\Framework\App\Config\Storage\WriterInterface
     */
    protected $_configInterface;

    /**
     * Config constructor.
     * @param ScopeConfigInterface $config
     * @param \Magento\Framework\App\Config\ConfigResource\ConfigInterface $configInterface
     */
    public function __construct(
        ScopeConfigInterface $config,
        \Magento\Framework\App\Config\ConfigResource\ConfigInterface $configInterface
    ) {
        $this->config = $config;
        $this->_configInterface = $configInterface;
    }

    /**
     * @return bool
     */
    public function getIsEnabled()
    {
        return $this->config->getValue(
            self::XML_PATH_ENABLED,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return array
     */
    public function getTimeRangeValue()
    {
        return $this->config->getValue(
            self::XML_PATH_TIME_RANGE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @param $value
     * @return \Magento\Framework\App\Config\ConfigResource\ConfigInterface
     */
    public function setIsUserViewEnabled($value)
    {
        return $this->_configInterface
            ->saveConfig(Config::PATH_IN_CORE_CONFIG_DATA, $value, 'default', 0);
    }
}
