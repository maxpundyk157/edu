<?php


namespace Divan\CustomReview\Model\Config\Source;

/**
 * Class CronAutoDeleteTimeRange
 * @package Divan\CustomReview\Model\Config\Source
 */
class CronAutoDeleteTimeRange implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * Options getter
     *
     * @return array
     */

    public function toOptionArray()
    {
        return [
            ['value' => '1 minutes', 'label' => __('1 minute')],
            ['value' => '2 minutes', 'label' => __('2 minutes')],
            ['value' => '30 minutes', 'label' => __('30 minutes')],
            ['value' => '1 days', 'label' => __('1 day')],
            ['value' => '5 days', 'label' => __('5 days')],
            ['value' => '30 days', 'label' => __('30 day')]
        ];
    }
}
