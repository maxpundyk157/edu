<?php


namespace Divan\CustomReview\Model\Review;

use Divan\CustomReview\Model\ResourceModel\Review\CollectionFactory;
use Divan\CustomReview\Model\Review;
use Divan\CustomReview\Api\Data\ReviewInterface;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Ui\DataProvider\Modifier\PoolInterface;
use Magento\Ui\DataProvider\ModifierPoolDataProvider;

/**
 * Class DataProvider
 * @package Divan\CustomReview\Model\Review
 */
class DataProvider extends ModifierPoolDataProvider
{
    /**
     * @var \Divan\CustomReview\Model\ResourceModel\Review\Collection
     */
    protected $collection;
    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;
    /**
     * @var array
     */
    protected $loadedData;
    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var \Magento\Framework\Registry
     */
    private $coreRegistry;

    /**
     * DataProvider constructor
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $reviewCollectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param StoreManagerInterface $storeManager
     * @param \Magento\Framework\Registry $coreRegistry
     * @param array $meta
     * @param array $data
     * @param PoolInterface|null $pool
     */

    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $reviewCollectionFactory,
        DataPersistorInterface $dataPersistor,
        StoreManagerInterface $storeManager,
        \Magento\Framework\Registry $coreRegistry,
        array $meta = [],
        array $data = [],
        PoolInterface $pool = null
    ) {
        $this->collection = $reviewCollectionFactory->create();
        $this->dataPersistor = $dataPersistor;
        $this->storeManager = $storeManager;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data, $pool);
        $this->coreRegistry = $coreRegistry;
    }

    /**
     * Get data
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }

        $items = $this->collection->getItems();
        /** @var ReviewInterface $review */
        foreach ($items as $review) {
            $this->loadedData[$review->getReviewId()] = $this->prepareData($review);
        }

        $data = $this->dataPersistor->get('review');
        if (!empty($data)) {
            $store = $this->collection->getNewEmptyItem();
            $store->setData($data);
            $this->loadedData[$store->getId()] = $store->getData();
            $this->dataPersistor->clear('review');
        }
        return $this->loadedData;
    }

    /**
     * @param ReviewInterface $review
     */
    private function prepareData($review)
    {
        try {
            return $review->getData();
        } catch (\Exception $exception) {
            return null;
        }
    }
}
