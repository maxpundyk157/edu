<?php

namespace Divan\CustomReview\Cron;

use Divan\CustomReview\Model\Config;
use Divan\CustomReview\Model\ReviewRepository;

/**
 * Class DeleteOldReviews
 * @package Divan\CustomReview\Cron
 */
class DeleteOldReviews
{
    private $reviewRepository;
    private $config;

    /**
     * DeleteOldReviews constructor.
     * @param ReviewRepository $reviewRepository
     * @param Config $config
     */
    public function __construct(
        ReviewRepository $reviewRepository,
        Config $config
    ) {
        $this->reviewRepository = $reviewRepository;
        $this->config = $config;
    }

    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        if ($this->config->getIsEnabled()) {
            $this->reviewRepository->deleteOldReviews();
        }
    }
}

