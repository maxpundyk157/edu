<?php

namespace Divan\CustomReview\Console\Command;

use Divan\CustomReview\Model\Config;
use Magento\Framework\Console\Cli;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ToggleReviewVisibility extends Command
{
    const INPUT_KEY_VALUE = 'value';

    private $config;

    public function __construct(
        Config $config
    ) {
        $this->config = $config;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setName('divan:review:toggle')
            ->addArgument(
                self::INPUT_KEY_VALUE,
                InputArgument::REQUIRED,
                'put value 1 or 2',
            );
        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $argument = $input->getArgument(self::INPUT_KEY_VALUE);
        if ($argument == 0 || $argument == 1) {
            $this->config->setIsUserViewEnabled($argument);
            if ($argument == 0) {
                $output->writeln('Review user view is DISABLED   ');
            } else {
                $output->writeln('Review user view is ENABLED   ');
            }
            return Cli::RETURN_SUCCESS;
        } else {
            $output->writeln('Please put correct value : 1 - is ENABLED, 0 - is DISABLED   ');
            return Cli::RETURN_FAILURE;
        }
    }
}
